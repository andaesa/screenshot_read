This repo contains the source code to take screenshot automatically and send to server(in this repo it is localhost server), from there the image will be process and every
word will be keep in MongoDB database.

This is not ready to be deployed on Google App Engine yet.
There is high possibility that same psqworker problem could happen. 

======== Some issue =======

1. Need to install tesseract version 4alpha. If installed by `brew install tesseract` the version installed is 3.05.

2. Some of the word that tesseract recognize is not readable. If the image contains few sentences, although it can detect the sentences/word and it is readable, there will 
   be some line of the sentences that tesseract could not detect. Thus ne of the solution is by installing tesseract version 4alpha.
