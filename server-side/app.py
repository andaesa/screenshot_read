from flask import Flask, request
from celery import Celery
from mongoalchemy.session import Session
from PIL import Image
from tesserocr import PyTessBaseAPI, RIL
from werkzeug.utils import secure_filename
from model import the_data
import os

app = Flask(__name__)
app.secret_key = 'super secret key'

app.config['UPLOAD_FOLDER'] = 'uploads/'
app.config['ALLOWED_EXTENSIONS'] = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app.config['CELERY_BROKER_URL'] = 'redis://localhost:6379/0'

celery = Celery('app', broker=app.config['CELERY_BROKER_URL'])
celery.conf.update(app.config)

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@celery.task
def process_image():
	""" open the image, do OCR Tesseract and save into database """
	#with app.app_context():
	session = Session.connect('OCRdb')
	session.clear_collection(the_data)
	image = Image.open('uploads/screenshot.png')
	with PyTessBaseAPI() as api:
		api.SetImage(image)
		boxes = api.GetComponentImages(RIL.TEXTLINE, True)
		#print 'Found {} textline image components.'.format(len(boxes))
		for i, (im, box, _, _) in enumerate(boxes):
			api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
			ocrResult = api.GetUTF8Text()
			conf = api.MeanTextConf()
			result = (u"text: {0}").format(ocrResult, **box)
			text_data = result.split(": ", 1)[1]

			data_save = the_data(Data=text_data)

			session.save(data_save)


@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'POST':
		file = request.files['file']
		if file and allowed_file(file.filename):
			filename = secure_filename(file.filename)
			file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

		if 'file' in request.files:
			process_image.delay()

	return 'Processing...'


if __name__ == '__main__':
	app.run(debug=True, port=8888)