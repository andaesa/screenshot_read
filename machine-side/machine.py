import pyscreenshot as ImageGrab
import schedule
import requests
import time

""" upload screenshot image to server """
def upload_img():
	im = ImageGrab.grab()
	im.save("screenshot.png")

	"""open the image to send it to the server """
	with open("screenshot.png", "rb") as image_file:
		url = "http://127.0.0.1:8888"
		files = {'file':image_file}
		response = requests.post(url, files=files)

	print response.status_code

""" upload image every 5 seconds """
schedule.every(5).seconds.do(upload_img)

while True:
	schedule.run_pending()
	time.sleep(1)
	
